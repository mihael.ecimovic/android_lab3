package rma.lv1

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.firestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import rma.lv1.ui.theme.LV1Theme
import java.security.AllPermission
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.compose.runtime.DisposableEffect
import androidx.compose.ui.platform.LocalContext


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LV1Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    NavHost(navController = navController, startDestination = "main_screen") {
                        composable("main_screen") {
                            MainScreen(navController = navController)
                        }
                        composable("step_counter") {
                            StepCounter(navController = navController)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun UserPreview(name: String, modifier: Modifier = Modifier) {
    val db = FirebaseFirestore.getInstance()
    var visina by remember { mutableStateOf(0f) }
    var tezina by remember { mutableStateOf(0f) }
    var bmi by remember { mutableStateOf(0f) }
    var newTezina by remember { mutableStateOf("") }
    var newVisina by remember { mutableStateOf("") }

    val formattedBmi = String.format("%.2f", bmi)

    fun fetchData() {
        db.collection("Bmi").document("bTvrdNKpaosSOaEu5GSW")
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    visina = document.getDouble("Visina")?.toFloat() ?: 0f
                    tezina = document.getDouble("Tezina")?.toFloat() ?: 0f
                    bmi = tezina / (visina * visina)
                }
            }
            .addOnFailureListener { e ->
                Log.e("MainActivity", "Error fetching document: $e")
            }
    }

    LaunchedEffect(Unit) {
        fetchData()
    }

    Text(
        text = "Pozdrav $name!",
        fontSize = 20.sp,
        lineHeight = 56.sp,
        modifier = Modifier
            .padding(top = 8.dp)
            .padding(start = 10.dp)
    )

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
    ) {
        Text(
            text = "Tvoj BMI je:",
            fontSize = 55.sp,
            lineHeight = 61.sp,
            textAlign = TextAlign.Center,
        )
        Text(
            text = formattedBmi,
            fontSize = 70.sp,
            lineHeight = 72.sp,
            fontWeight = FontWeight.Bold,
        )
        TextField(
            value = newVisina,
            onValueChange = { newVisina = it },
            label = { Text("Nova Visina:") },
            modifier = Modifier.fillMaxWidth(0.8f)
        )
        Button(onClick = {
            val visinaValue = newVisina.toFloatOrNull()
            if (visinaValue != null) {
                db.collection("Bmi").document("bTvrdNKpaosSOaEu5GSW")
                    .update("Visina", visinaValue)
                    .addOnSuccessListener {
                        fetchData()
                    }
                    .addOnFailureListener { e ->
                        Log.e("MainActivity", "Error updating Visina: $e")
                    }
            }
        }) {
            Text("Unesi Visinu")
        }
        TextField(
            value = newTezina,
            onValueChange = { newTezina = it },
            label = { Text("Nova Tezina:") },
            modifier = Modifier.fillMaxWidth(0.8f)
        )
        Button(onClick = {
            val tezinaValue = newTezina.toFloatOrNull()
            if (tezinaValue != null) {
                db.collection("Bmi").document("bTvrdNKpaosSOaEu5GSW")
                    .update("Tezina", tezinaValue)
                    .addOnSuccessListener {
                        fetchData()
                    }
                    .addOnFailureListener { e ->
                        Log.e("MainActivity", "Error updating Tezina: $e")
                    }
            }
        }) {
            Text("Unesi Tezinu")
        }
    }
}


@Composable
fun BackgroundImage(modifier: Modifier) {
    Box(modifier = modifier) {
        Image(
            painter = painterResource(id = R.drawable.fitness),
            contentDescription = null,
            contentScale = ContentScale.Crop,
            alpha = 0.1F
        )
    }
}

@Composable
fun MainScreen(navController: NavController) {
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        BackgroundImage(modifier = Modifier.fillMaxSize())
        UserPreview(name = "Mihael")

        Button(
            onClick = {
                navController.navigate("step_counter")
            },
            modifier = Modifier
                .align(Alignment.BottomEnd)
                .padding(16.dp)
        ) {
            Text(text = "Step Counter")
        }
    }
}

@Composable
fun StepCounter(navController: NavController) {
    val context = LocalContext.current
    val sensorManager = remember { context.getSystemService(Context.SENSOR_SERVICE) as SensorManager }
    val sensor = remember { sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) }
    var stepCount by remember { mutableStateOf(0) }

    val sensorEventListener = remember {
        object : SensorEventListener {
            override fun onSensorChanged(event: SensorEvent) {
                if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
                    val x = event.values[0]
                    val y = event.values[1]
                    val z = event.values[2]

                    val acceleration = Math.sqrt((x * x + y * y + z * z).toDouble())
                    if (acceleration > 12) {
                        stepCount++
                    }
                }
            }
            override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
            }
        }
    }

    DisposableEffect(sensorManager) {
        sensorManager.registerListener(sensorEventListener, sensor, SensorManager.SENSOR_DELAY_NORMAL)
        onDispose {
            sensorManager.unregisterListener(sensorEventListener)
        }
    }

    val db = FirebaseFirestore.getInstance()
    LaunchedEffect(stepCount) {
        db.collection("Bmi").document("bTvrdNKpaosSOaEu5GSW")
            .update("Steps", stepCount)
            .addOnSuccessListener {
                Log.d("StepCounter", "Step count updated successfully")
            }
            .addOnFailureListener { e ->
                Log.e("StepCounter", "Error updating step count: $e")
            }
    }

    Box(modifier = Modifier.fillMaxSize()) {
        BackgroundImage(modifier = Modifier.fillMaxSize())
        Column {
            Text(
                text = "Step Count: $stepCount",
                fontSize = 20.sp
            )
        }
        // Back button
        Button(
            onClick = { navController.popBackStack() },
            modifier = Modifier
                .align(Alignment.BottomStart)
                .padding(16.dp)
        ) {
            Text("User Info")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewMainScreen() {
    LV1Theme {
        MainScreen(navController = rememberNavController())
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewStepCounter() {
    LV1Theme {
        StepCounter(navController = rememberNavController())
    }
}


